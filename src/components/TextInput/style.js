import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    input: {
        fontSize: 20,
        color: "#6c63ff",
        margin: 8,
        width: "88%",
        borderWidth: 2,
        borderRadius: 20,
        borderColor: "#6c63ff",
        paddingHorizontal: 15,
        paddingVertical: 6,
        backgroundColor: "white",
        fontFamily: "",
    },
});
