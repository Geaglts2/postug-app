import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    button: {
        color: "white",
        backgroundColor: "#6c63ff",
        fontSize: 24,
        margin: 11,
        paddingHorizontal: 15,
        paddingVertical: 7,
        borderRadius: 50,
        textAlign: "center",
    },
});
