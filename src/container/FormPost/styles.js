import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    imageContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 5,
    },
    inputsContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 4,
    },
    buttonContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 2,
    },
    salirContainer: {
        justifyContent: "center",
    },
});
